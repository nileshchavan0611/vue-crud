import axios from 'axios';
const API_URL = 'http://dummy.restapiexample.com';
export class APIService {

    constructor() {
        axios.defaults.headers.common['JSESSIONID'] = 123;
    }
    getEmpData() {
        const url = `${API_URL}/api/v1/employees`;
        return axios.get(url).then(response => response.data);
    }
    getEmpDataById(id) {
        const url = `${API_URL}/api/v1/employee/${id}`;
        return axios.get(url).then(response => response.data);
    }
    deleteEmp(id) {
        const url = `${API_URL}/api/v1/delete/${id}`;
        return axios.delete(url).then(response => response.data);
    }
    updateEmp(sendData, id) {
        const url = `${API_URL}/api/v1/update/${id}`;
        return axios.put(url, sendData).then(response => response.data);
    }
    createEmp(sendData) {
        const url = `${API_URL}/api/v1/create`;
        return axios.post(url, sendData).then(response => response.data);
    }
}
