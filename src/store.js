import Vue from 'vue'
import Vuex from 'vuex'
import { APIService } from './APIService';

const apiService = new APIService();
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    employees: [],
    status: false
  },
  getters: {
    getEmpById: (state) => (id) => {
      return state.employees.find(emp => {
        return (emp.id === id)
      })
    },
    getStatus: (state) => {
      return state.status
    }

  },
  mutations: {
    SET_EMPLOYEES(state, empData) {
      state.employees = empData
      state.status = true
    }

  },
  actions: {
    loadEmpData({ commit }, empData) {
      if (empData) {
        commit('SET_EMPLOYEES', empData)
      }
      else {
        apiService.getEmpData().then(empData => {
          commit('SET_EMPLOYEES', empData)
        })
      }
    }
  }
})
