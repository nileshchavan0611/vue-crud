import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Employee/Home.vue'
import Edit from './views/Employee/EmployeeForm.vue'
import EmployeeDelete from './views/Employee/Delete.vue'
import EmployeeUpdate from './views/Employee/Update.vue'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/employee', component: Home,name:'employee'},
    { path: '/edit/:id', component: Edit ,name:'employeeEdit'},
    { path: '/create', component: Edit ,name:'employeeCreate'},
    { path: '/delete/:id', component: EmployeeDelete },
    { path: '/update/:id', component: EmployeeUpdate }
  ]
})
